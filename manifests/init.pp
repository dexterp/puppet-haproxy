class haproxy($service_enable = true, $service_ensure = 'running') {
    include concat::setup
    concat{ '/etc/haproxy/haproxy.cfg':
        notify => Service[ 'haproxy' ]
    }

    package {
        'haproxy':
            ensure => installed;
        'pound':
            ensure => installed;
    }

    file { '/etc/default/haproxy':
        source => 'puppet:///modules/haproxy/haproxy_default',
        require => Package[ 'haproxy' ]
    }

    service {
        'haproxy':
            ensure => $service_ensure,
            hasstatus => true,
            hasrestart => true,
            enable => $service_enable,
            subscribe => Concat[ '/etc/haproxy/haproxy.cfg' ]
    }

    define global(
        $chroot = false,
        $daemon = true,
        $gid = false,
        $group = 'haproxy',
        $log = [ '127.0.0.1    local0', '127.0.0.1    local1' ],
        $log_send_hostname = false,
        $nbproc = 1,
        $pidfile = false,
        $ulimit_n = false,
        $user = 'haproxy',
        $stats = false,
        $node = false,
        $description = false,
        $maxconn = 4096,
        $maxpipes = false,
        $noepoll = false,
        $nokqueue = false,
        $nosepoll = false,
        $nosplice = false,
        $spread_checks = false,
        $tune_bufsize = false,
        $tune_chksize = false,
        $tune_maxaccept = false,
        $tune_maxpollevents = false,
        $tune_maxrewrite = false,
        $tune_rcvbuf_client = false,
        $tune_rcvbuf_server = false,
        $tune_sndbuf_client = false,
        $tune_sndbuf_server = false,
        $debug = false,
        $quiet = false
    ) {
        concat::fragment{ "frag.global":
            target => '/etc/haproxy/haproxy.cfg',
            order => 00,
            content => template('haproxy/global')
        }
    }

    define defaults(
        $log = global,
        $mode = http,
        $option = false,
        $nbproc = false,
        $user = 'haproxy',
        $group = 'haproxy',
        $retries = 3,
        $maxconn = 4096,
        $contimeout = 5000,
        $clitimeout = 50000,
        $srvtimeout = 50000,
        $daemon = true,
        $debug = false,
        $quiet = false
    ) {
        concat::fragment{ "frag.defaults":
            target => '/etc/haproxy/haproxy.cfg',
            order => 10,
            content => template('haproxy/defaults')
        }
    }

    define listener(
        $vip,
        $port,
        $balance = roundrobin,
        $mode = false,
        $stats = false,
        $log = false,
        $cookie = false,
        $server = false,
        $option = false
    ) {
        concat::fragment{ "frag.listener":
            target => '/etc/haproxy/haproxy.cfg',
            order => 20,
            content => template('haproxy/listener')
        }
    }
}
